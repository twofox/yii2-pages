<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_123830_create_post_translation_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%post_translation}}', [
            'post_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'language' => Schema::TYPE_STRING . '(16) NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'keywords' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT . ' NOT NULL',
        ]);
        $this->addPrimaryKey('', '{{%post_translation}}', ['post_id', 'language']);
    }

    public function down()
    {
        $this->dropTable('{{%post_translation}}');
    }
}