<?php

namespace twofox\pages\models;

use Yii;
use twofox\pages\Module;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the model class for table "{{%pages}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property integer $published
 * @property string $content
 * @property string $title_browser
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $created_at
 * @property string $updated_at
 * 
 * @author Belosludcev Vasilij <bupy765@gmail.com>
 * @since 1.0.0
 */
class Page extends ActiveRecord
{
    
    /**
     * Value of 'published' field where page is not published.
     */
    const PUBLISHED_NO = 0;
    /**
     * Value of 'published' field where page is published.
     */
    const PUBLISHED_YES = 1;
    
    /**
     * @inheritdoc
     */
     
    
    public static function getStatusArray()
    {
        return [
            self::PUBLISHED_NO => Module::t('NOT ACTIVE'),
            self::PUBLISHED_YES => Module::t('ACTIVE')
        ];
    } 
     
     
    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'immutable' => true,
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['content', 'meta_title', 'meta_keywords', 'meta_description'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return Yii::$app->getModule('pages')->tableName;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['published'], 'boolean'],
            [['content'], 'string', 'max' => 65535],
            [['title', 'slug'], 'string', 'max' => 255],
            [['meta_keywords', 'meta_description', 'meta_title'], 'string', 'max' => 255],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('ID'),
            'title' => Module::t('TITLE'),
            'slug' => Module::t('Slug'),
            'published' => Module::t('PUBLISHED'),
            'content' => Module::t('CONTENT'),
            'meta_title' => Module::t('META TITLE'),
            'meta_keywords' => Module::t('META KEYWORDS'),
            'meta_description' => Module::t('META KEYWORDS'),
            'created_at' => Module::t('CREATED AT'),
            'updated_at' => Module::t('UPDATED AT'),
        ];
    }
    
    /**
     * List values of field 'published' with label.
     * @return array
     */
    static public function publishedDropDownList()
    {
        $formatter = Yii::$app->formatter;
        return [
            self::PUBLISHED_NO => $formatter->asBoolean(self::PUBLISHED_NO),
            self::PUBLISHED_YES => $formatter->asBoolean(self::PUBLISHED_YES),
        ];
    }
    
    public function transactions(){
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    
    public function getTranslations(){
        return $this->hasMany(PostTranslation::className(), ['post_id' => 'id'])->where(['class' => self::className()]);
    }
    
    

    /**
     * Formats all model errors into a single string
     * @return string
     */
    public function formatErrors()
    {
        $result = '';
        foreach($this->getErrors() as $attribute => $errors) {
            $result .= implode(" ", $errors)." ";
        }
        return $result;
    }
    
}
